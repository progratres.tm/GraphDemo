import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

public class MainGraph extends ApplicationFrame
{
	public MainGraph (String applicationTitle, String charTitle)
	{
		super(applicationTitle);
		JFreeChart lineChart = ChartFactory.createLineChart(
				charTitle,
				"A�os","N�mero de escuelas"
				,createDataset()
				,PlotOrientation.VERTICAL
				,true,true,false
				);
		ChartPanel chartPanel = new ChartPanel( lineChart );
		chartPanel.setPreferredSize( new java.awt.Dimension(560,367));
		setContentPane(chartPanel);		
	}

	private DefaultCategoryDataset createDataset() 
	{
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		dataset.addValue(15,"Escuelas1","1960");
		dataset.addValue(30,"Escuelas1","1980");
		dataset.addValue(200,"Escuelas1","2000");
		dataset.addValue(250,"Escuelas1","2010");
		dataset.addValue(10,"Escuelas","1960");
		dataset.addValue(120,"Escuelas","2000");
		dataset.addValue(240,"Escuelas","2010");
		dataset.addValue(300,"Escuelas","2017");
		
		return dataset;
	}
	public static void main (String[] args)
	{
		MainGraph chart = new MainGraph(
				"Escuela vs a�os"
				,"Numero de escuelas vs A�os");
		chart.pack();
		RefineryUtilities.centerFrameOnScreen(chart);
		chart.setVisible(true);
		
		pieGraph chartPie = new pieGraph(
				"Numero de escuelas vs A�os");
		chart.pack();
		RefineryUtilities.centerFrameOnScreen(chartPie);
		chart.setVisible(true);

	}
}
