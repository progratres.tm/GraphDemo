import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.ApplicationFrame;


public class pieGraph extends ApplicationFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public pieGraph (String charTitle)
	{
		super("Grtafico de Torta");
		JFreeChart pieChart = ChartFactory.createPieChart(
				charTitle
				,createDataset2()
				,true,true,false
				);
		ChartPanel chartPanel = new ChartPanel( pieChart );
		chartPanel.setPreferredSize( new java.awt.Dimension(560,367));
		setContentPane(chartPanel);

	}
	private PieDataset createDataset2() 
	{
		DefaultPieDataset dataset = new DefaultPieDataset( );
	      dataset.setValue( "IPhone 5s" , new Double( 20 ) );  
	      dataset.setValue( "SamSung Grand" , new Double( 20 ) );   
	      dataset.setValue( "MotoG" , new Double( 40 ) );    
	      dataset.setValue( "Nokia Lumia" , new Double( 10 ) );  
      return dataset;         		
	}	

}
